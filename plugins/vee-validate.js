import Vue from 'vue'
import { extend, ValidationObserver, ValidationProvider } from 'vee-validate'
import * as rules from 'vee-validate/dist/rules'
import { messages } from 'vee-validate/dist/locale/en.json'
import Vuelidate from 'vuelidate' //.. 

Vue.use(Vuelidate)

Object.keys(rules).forEach(rule => {
    extend(rule, {
        // eslint-disable-next-line import/namespace
        ...rules[rule],
        message: messages[rule]
    })
})
Vue.use(VeeValidate);
VeeValidate.Validator.extend("polite", {
    getMessage: field => `You need to be polite in the ${field} field`,
    validate: value => value.toLowerCase().indexOf("please") !== -1
});
new Vue({
    el: "#app",
    data: {
        form: {
            name: "",
            message: "",
            inquiry_type: "",
            logrocket_usecases: [],
            terms: false,
            concepts: [],
            js_awesome: ""
        },
        options: {
            inquiry: [
                { value: "feature", text: "Feature Request" },
                { value: "bug", text: "Bug Report" },
                { value: "support", text: "Support" }
            ]
        }
    }
});

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)